package routes

import (
	. "awesomeProject/auth"
	"awesomeProject/controllers/todo"
	"awesomeProject/controllers/user"
	"github.com/gorilla/mux"
	"net/http"
)

func Handlers() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	r.Use(CommonMiddleware)
	r.HandleFunc("/", user.TestAPI).Methods("GET")
	r.HandleFunc("/register", user.CreateUser).Methods("POST")
	r.HandleFunc("/login", user.Login).Methods("POST")

	s := r.PathPrefix("/auth").Subrouter()
	s.Use(JwtVerify)
	s.HandleFunc("/todo", todo.GetAllTodoList).Methods("GET")
	s.HandleFunc("/todo", todo.AddNewTodo).Methods("POST")
	s.HandleFunc("/todo/{id}", todo.UpdateTodoById).Methods("PUT")
	s.HandleFunc("/todo/{id}", todo.DeleteTodoById).Methods("DELETE")
	return r
}

func CommonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "http://localhost:4200")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, User-Agent, Referer, Cache-Control, X-header")
		next.ServeHTTP(w, r)
	})
}
