package utils

import (
	"awesomeProject/models"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
)

func ConnectDB() *gorm.DB {

	db, err := gorm.Open("mysql", "root:root@tcp(127.0.0.1:3306)/todolist?charset=utf8&parseTime=True")
	if err != nil {
		log.Fatal(err)
	}

	db.AutoMigrate(&models.User{})
	db.AutoMigrate(&models.Todo{})

	fmt.Println("Successfully connected!", db)
	return db
}
