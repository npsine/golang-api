package models

import "github.com/jinzhu/gorm"

type Todo struct {
	gorm.Model

	Userid   int `json:"Userid"`
	Todo     string `json:"Todo"`
	Finished bool   `gorm:"type:boolean;default:false;"`
}
