package todo

import (
	"awesomeProject/models"
	"awesomeProject/utils"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"net/http"
)

var db = utils.ConnectDB()

func GetAllTodoList(w http.ResponseWriter, r *http.Request) {
	tokenString := r.Header.Get("x-access-token")
	token, _, err := new(jwt.Parser).ParseUnverified(tokenString, jwt.MapClaims{})

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		userId, _ := fmt.Println(claims["UserID"])
		println(userId)
		var todoList []models.Todo
		db.Find(&todoList,models.Todo{Userid: userId})
		json.NewEncoder(w).Encode(todoList)
	} else {
		fmt.Println(err)
	}
}

func AddNewTodo(w http.ResponseWriter, r *http.Request) {
	todo := &models.Todo{}
	json.NewDecoder(r.Body).Decode(todo)
	createdTodo := db.Create(todo)

	var errMessage = createdTodo.Error
	if createdTodo.Error != nil {
		fmt.Println(errMessage)
	}

	json.NewEncoder(w).Encode(createdTodo)
}

func UpdateTodoById(w http.ResponseWriter, r *http.Request) {
	todo := &models.Todo{}
	params := mux.Vars(r)
	var id = params["id"]

	db.First(&todo, id)
	json.NewDecoder(r.Body).Decode(todo)
	db.Save(&todo)
	json.NewEncoder(w).Encode(&todo)
}

func DeleteTodoById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var id = params["id"]
	var todo models.Todo
	db.First(&todo, id)
	db.Delete(&todo)
	json.NewEncoder(w).Encode("Todo deleted")
}
