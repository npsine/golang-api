module awesomeProject

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.14
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.7.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
)
